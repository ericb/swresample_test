# Makefile générique
# Eric Bachard / 26th December 2023
# This file is under GPL v3 License

# First, identify the current OS
MY_OS = $(shell uname -s)

#uncomment to build both stripped and not stripped down binaries
#ADD_DEBUG_VERSION = -DBUILDING_DEBUG_VERSION_TOO

#GCC_VERSION=-10
CC = gcc${GCC_VERSION}
CXX = g++${GCC_VERSION}
INCLUDE_DIR = ./inc  -I/usr/local/include
BUILD_DIR = build
SOURCES_DIR = src
APPLICATION_NAME = swresample_test
FILENAME = ${BUILD_DIR}/${APPLICATION_NAME}

#FSTACK=-fstack-usage

WARNINGS = -Wall -Wwrite-strings -Wextra

OPTIMIZATION_FLAG = -O2

SECURITY =  -Wno-conversion-null \
            -Wuseless-cast \
            -fstack-protector-all

G++_SECURITY_FLAGS = \
            -fstack-protector \
            -pie \
            -fPIE \
            -D_FORTIFY_SOURCE=2 \
            -Wformat \
            -Wformat-security

SECURITY += ${G++_SECURITY_FLAGS}

#EXTRA_FEATURES = 
#FSANITIZE=-fsanitize-undefined-trap-on-error
#FSANITIZE=-fsanitize=leak

CXXFLAGS = -Wall -ansi -std=c++17 ${OPTIMIZATION_FLAG} ${WARNINGS} ${SECURITY} ${FSANITIZE} ${FSTACK}

OPENCV_CFLAGS = $(shell pkg-config --cflags --libs opencv)
FFMPEG_CFLAGS = $(shell pkg-config --cflags --libs libavcodec libavformat libavfilter libswresample libavutil)
SDL2_CFLAGS   = $(shell sdl2-config --cflags --libs)

LDFLAGS += \
        -L/usr/local/lib \
        ${OPENCV_CFLAGS} \
        ${FFMPEG_CFLAGS} \
        ${SDL2_CFLAGS} \
        -lstdc++ -ldl -L/usr/lib  -lpthread -lz -lswscale -lm

OBJS = \
        ${SOURCES_DIR}/swresample_test.c

CFLAGS_DEBUG = -g -DDEBUG
DEBUG_LEVEL_VALUE = 0
DEBUG=-DDEBUG_LEVEL=${DEBUG_LEVEL_VALUE} ${ADD_DEBUG_VERSION}


ifndef ADD_DEBUG_VERSION
all : clean ${FILENAME}
else
all : clean ${FILENAME} ${FILENAME}_debug
endif

${FILENAME}: ${OBJS}
	${CXX} -I${INCLUDE_DIR} ${DEBUG} ${NO_MAIN} ${EXTRA_FEATURES} -D${MY_OS} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
	strip ${FILENAME}
	echo "Build completed"

${FILENAME}_debug: ${OBJS}
	${CXX}  ${DEBUG} ${NO_MAIN} ${EXTRA_FEATURES} -D${MY_OS} -I${INCLUDE_DIR} ${CXXFLAGS} ${CFLAGS_DEBUG} -o $@ $^ ${LDFLAGS}

clean:
	${RM} ${BUILD_DIR}/*.o ${FILENAME} ${FILENAME}_debug
