Copyright : Eric Bachard 
lun. 08 janv. 2024 14:56:58 CET

Tool to convert several AV_SAMPLE_FMT int other, adapted to ffmpeg 4.4

Original author of the code are (cf copyright) Michael Niedermayer, Fabrice Bellard.

Source : https://spirton.com/svn/MPlayer-SB/ffmpeg/libswresample/swresample_test.c

License of the current repository : GPL v3



Build (Linux only) :

1. cd inc folder 
2. replace the current config.h with your own (located in ffmpeg sources root dir)
3. supposing make is installed, type : 

 make
 cd ./build

And finaly type :
 
 ./swresample -h



